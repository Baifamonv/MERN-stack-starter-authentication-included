import axios from 'axios';
import {
    AUTH_USER,
    AUTH_ERROR,
    UNAUTH_USER,
    FETCH_MESSAGE,
    ADD_ITEM,
    FETCH_LIST,
} from './types';
const ROOT_URL = 'http://localhost:3090';

export function signinUser({ email, password }, history) {
    return function(dispatch){
        axios.post(`${ ROOT_URL }/signin`, { email, password })
        .then(res => {
            dispatch({ type: AUTH_USER });
            localStorage.setItem('token', res.data.token);
            history.push('/');
        })
        .catch(() => {
            dispatch(authError('bad login info'));
        })
    }
};

export function signupUser({ email, password }, history) {
    return function(dispatch){
        axios.post(`${ ROOT_URL }/signup`, { email, password })
        .then(res => {
            dispatch({ type: AUTH_USER });
            localStorage.setItem('token', res.data.token);
            history.push('/');
        })
        .catch(res => {
            dispatch(authError(res.response.data.error));
        })
    }
};

export function addItem({ value }) {
    return function(dispatch){
        axios.post(`${ ROOT_URL }/add`, { value })
        .then(response => {
            dispatch({ type: ADD_ITEM , payload: response.data.items });
        })
        .catch(res => {
            dispatch(authError(response.data.error));
        })
    }
};

export function fetchList() {
    return function(dispatch){
        axios.get(`${ ROOT_URL }/get`)
        .then(response => {
            dispatch({ type: FETCH_LIST , payload: response.data.items });
        })
        .catch(res => {
            dispatch(authError(response.data.error));
        })
    }
};

export function signoutUser() {
    localStorage.removeItem('token');
    return { type: UNAUTH_USER };
};

export function authError(err){
   return {
       type: AUTH_ERROR,
       err
   };
};

export function fetchMessage(){
    return function(dispatch){
        axios.get(ROOT_URL, {
            headers: { authorization: localStorage.getItem('token')}
        })
            .then(response => {
                dispatch({
                    type: FETCH_MESSAGE,
                    payload: response.data.message
                });
            });
    }
}
