import React, { Component } from 'react';
import Header from './components/header'
import { BrowserRouter } from 'react-router-dom';
import Routes from './routes';
import { hot } from 'react-hot-loader'

const App = () => (
    <BrowserRouter>
        <main>
            <Header />
            <Routes />
        </main>
    </BrowserRouter>
);

export default hot(module)(App);
