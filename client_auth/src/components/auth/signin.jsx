import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';
import { connect } from 'react-redux';
import * as actions from '../../actions';

class Signin extends Component {
    handleFormSubmit = ({ email, password }) => {
        this.props.signinUser({ email, password }, this.props.history);
    }

    renderInput = field => {
        if(!field.className) { field.className = "form-control" }
        if(!field.type) { field.type = "text" }
        return (
            <Field
                name={ field.name }
                id={ field.name }
                type={ field.type }
                className={ field.className }
                component="input"
            />
        )
    }
    render = () => {
        const { handleSubmit, errorMessage } = this.props
        return (
            <form onSubmit={ handleSubmit(this.handleFormSubmit) }>
                <fieldset className="form-group">
                    <label>Email:</label>
                    { this.renderInput({ name: "email", type: "email" }) }
                </fieldset>
                <fieldset className="form-group">
                    <label>Password:</label>
                    { this.renderInput({ name: "password", type: "password" }) }
                </fieldset>
                { errorMessage &&
                    <div className='alert alert-danger'>
                        errorMessage
                    </div>
                }
                <button action="submit" className="btn btn-primary">Sign in</button>
            </form>
        )
    }
}
const reduxFormSettings = reduxForm({
    form: 'signin'
})(Signin);

const mapStateToProps = state => ({
    errorMessage: state.auth.err
});
export default connect(mapStateToProps, actions)(reduxFormSettings);
