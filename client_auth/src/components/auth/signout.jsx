import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions';

class Signout extends Component {

    componentWillMount = () => this.props.signoutUser();

    render = () => (
        <div>
            sorry to see you go
        </div>
    );
}

export default connect(undefined, actions)(Signout);
