import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import * as actions from '../../actions'

const renderField = ({ input, label, type, meta: { touched, error, warning } }) => (
    <fieldset className="form-group">
        <label htmlFor={ input.name }>{ label }</label>
        <input className="form-control" { ...input } type={ type }/>
        { touched && error && <span className="text-danger">{ error }</span> }
    </fieldset>
)

class SignUp extends Component {
    handleFormSubmit = ({ email, password }) => {
        this.props.signupUser({ email, password }, this.props.history)
    }
    render = () => {
    const { handleSubmit, errorMessage } = this.props;

    return (
        <form onSubmit={ handleSubmit(this.handleFormSubmit) }>
            <Field name="email" component={ renderField } type="email" label="Email"/>
            <Field name="password" component={ renderField } type="password" label="Password"/>
            <Field
                name="password_confirmation"
                component={ renderField }
                type="password"
                label="Password Confirmation"
            />
            <button type="submit" className="btn btn-primary">Sign Up</button>
            <div className="text-danger">{ this.props.errorMessage }</div>
        </form>
    );
  }
}

const validate = (values) => {
    let errors = {}
    if (!values.email){
        errors.email = 'please enter an email';
    }
    if (!values.password){
        errors.password = 'please enter a password';
    }
    if (!values.password_confirmation){
        errors.password_confirmation = 'please enter a password confirmation';
    }
    if (values.password != values.password_confirmation) {
        errors.password = 'Password and password confirmation don\'t match!'
    }
    return errors
}

const mapStateToProps = state => ({ errorMessage: state.auth.err })

export default connect(mapStateToProps, actions)(reduxForm({
  form:'SignUp',
  validate
})(SignUp));
