import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from  'react-redux';

const Header = ({ authenticated }) => (
    <React.Fragment>
            { authenticated
                ? <Link to='/signout'>Sign out</Link>
                : <ul>
                    <Router linkTo='/' label='HOME' />
                    <Router linkTo='/signin' label='SIGN IN' />
                    <Router linkTo='/signup' label='SIGN UP' />
                </ul>
            }
    </React.Fragment>
);

const Router = ({ linkTo, label }) => (
    <li>
        <Link to={ linkTo }>{ label }</Link>
    </li>
);

const mapStateToProps = state => ({
    authenticated: state.auth.authenticated,
});
export default connect(mapStateToProps)(Header);
