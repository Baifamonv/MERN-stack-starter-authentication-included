import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';
import { Link } from 'react-router-dom';

class List extends Component {
    state = {
        value: ''
    };

    componentWillMount = () => this.props.fetchList();

    handleInput = e => this.setState({
        value: e.target.value
    });

    saveInput = () => {
        this.props.addItem({
            value: this.state.value
        });
        this.setState({
            value: ''
        });
    }

    detectEnter = e => {
        if (e.keyCode === 13 || e.charCode === 13) this.saveInput();
    };

    render = () => {
        const { list, authenticated } = this.props;
        return (
            <div>
                { authenticated
                    ?<div>
                        <input
                            placeholder='Enter something'
                            value={ this.state.value }
                            onChange={ this.handleInput }
                            onKeyPress={ this.detectEnter }
                        />
                        <button onClick={ this.saveInput }> submit </button>
                    </div>
                    : <Link to='/signin'>Sign in to Add more</Link>
                }
                { list.map(item =>
                    <li key={ item._id }>
                        { item.value }
                    </li>
                )}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    list: state.list.list,
    authenticated: state.auth.authenticated
});

export default connect(mapStateToProps, actions)(List);
