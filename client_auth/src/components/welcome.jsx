import React from 'react';
import List from './list';

const Welcome = () => (
    <div>
        <h1> Welcome to To Do List</h1>
        <List />
    </div>
);

export default Welcome;
