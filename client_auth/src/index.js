import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';

import reducers from './reducers';
import reduxThunk from 'redux-thunk';
import App from './app';
import { AUTH_USER } from './actions/types';

const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore);
const store = createStoreWithMiddleware(reducers);
// automatically authenticate user if there is a token
const token = localStorage.getItem('token');

if (token) {
    store.dispatch({ type: AUTH_USER });
}

const render = (Component) => {
    ReactDOM.render(
        <Provider store={ store }>
            <Component />
        </Provider>
        , document.querySelector('.container')
    );

};

render(App);
