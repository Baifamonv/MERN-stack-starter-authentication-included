import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';
import authReducer from './auth_reducer';
import listReducer from './list';

const rootReducer = combineReducers({
    form,
    auth: authReducer,
    list: listReducer,
});

export default rootReducer;
