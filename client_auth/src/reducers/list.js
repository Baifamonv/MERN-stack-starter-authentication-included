import {
    ADD_ITEM,
    FETCH_LIST,
} from '../actions/types';

const initialState = {
    list: [],
}
export default function(state = initialState, action){
    switch(action.type){
        case ADD_ITEM:
            return {
                ...state,
                list: action.payload
            }

        case FETCH_LIST:
            return {
                ...state,
                list: action.payload
            }
    }
    return state;
}
