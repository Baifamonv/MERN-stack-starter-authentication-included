const Item = require('../models/item');

exports.add = function(req, res, next){
    const value = req.body.value;
    if (!value){
        return res.status(422).send({ error: 'must provide some value' });
    }
    const item = new Item({
        value
    });

    item.save().then(Item.find({}).then(function(items){
        res.json({ items });
    }));
}

exports.get = function(req, res){
    Item.find({}).then(function(items){
        res.json({ items });
    });
}

