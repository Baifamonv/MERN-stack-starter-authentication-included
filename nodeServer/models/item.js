const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//define models
const itemSchema = new Schema({
    value: String
});

//create the model class
const ModelClass = mongoose.model('item', itemSchema);

//export the model
module.exports = ModelClass;
