const Authentication = require('./controllers/authentication');
const List = require('./controllers/list');
const passportService = require('./services/passport');
const passport = require('passport');

const requireAuth = passport.authenticate('jwt', { session: false });
const requireSignin = passport.authenticate('local', { session: false });

module.exports = function(app){
    app.post('/add', List.add)
    app.get('/get', List.get)
    app.get('/', requireAuth, function(req, res){
        res.send({ message: 'super secret' });
    })
    app.post('/signin', requireSignin, Authentication.signin)
    app.post('/signup', Authentication.signup)
}
